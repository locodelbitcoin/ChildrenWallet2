pragma solidity ^0.4.4;

contract ChildrenWallet {
  WalletAllowed[] public allowed;
  address parents;
  // address public owner;
  // uint public mostSent;

  // mapping (address => uint) pendingWithdraws;

  struct WalletAllowed {
    bytes32 name;
    address account;
  }
  
  function ChildrenWallet() public {
    parents = msg.sender; // emisor del mensaje (llamada actual)
    // mostSent = msg.value;
    // owner = msg.sender;
  }

  function addAllowed(bytes32 _name, address _account) public returns (bool _success) {

    WalletAllowed memory newAllowed;
    newAllowed.name = _name;
    newAllowed.account = _account;

    allowed.push(newAllowed);
    return true;
  }

  function getAllowedNames() public constant returns (bytes32[]) {    
      uint length = allowed.length;
      bytes32[] memory names = new bytes32[](length);

      for (uint i = 0; i < length; i++) {
          names[i] = allowed[i].name;
      }

      return names;
  }

  function getAllowedAddresses() public constant returns (address[]) {    
      uint length = allowed.length;
      address[] memory addresses = new address[](length);

      for (uint i = 0; i < length; i++) {
          addresses[i] = allowed[i].account;
      }

      return addresses;
  }

  function buySomething(address _to) public constant returns (string) {   
      uint length = allowed.length;
      for (uint i = 0; i < length; i++) {
          if (_to == allowed[i].account) {
            return "You can buy here";
          }
      } 
      return "You can not buy here";    
  }

  function getParentBalance() public constant returns (uint) {
      return parents.balance;
  }
  
  // saca la direccion del contrato
  function getContractAddress() public constant returns (address) {
      return this;
  }
  
  //saca balance del contrato
   function getContractBalance() public constant returns (uint256) {
      return this.balance;
  }
  // envia las monedas que se quieren depositar en el contrato
   function sendToContract() payable public returns (bool) {
        return true;
    }
}
