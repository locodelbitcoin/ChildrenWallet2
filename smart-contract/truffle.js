module.exports = {
    networks : {
        development : {
            gas : 2500000,
            from : "0x5aeda56215b167893e80b4fe645ba6d5bab767de",
            host : "localhost",
            port : 9545,
            network_id : "*" // Match any network id
        }
    }
};
